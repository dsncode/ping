package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Response struct {
	Message string
}

func main() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/", ping)

	// Start server
	e.Logger.Fatal(e.Start(":8000"))
}

// Handler
func ping(c echo.Context) error {
	return c.JSON(http.StatusOK, Response{
		Message: "Pong",
	})
}
